function populateUFs()
{
    const ufSelect = document.querySelector("select[name=uf]")
    
    fetch("https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome")
    .then( res => res.json() )  // funcao anonima nao precisa das chaves, return e parenteses extras
    .then( states =>
    {
        for (const state of states)
        {
            ufSelect.innerHTML += `<option value="${state.id}">${state.nome}</option>`
        }
    } )
}

function getCities(event)
{
    const citySelect = document.querySelector("[name=city]")
    const stateInput = document.querySelector("[name=state]")
    
    const ufValue = event.target.value
    
    const indexOfSelectedState = event.target.selectedIndex
    stateInput.value = event.target.options[indexOfSelectedState].text
    
    const url = `https://servicodados.ibge.gov.br/api/v1/localidades/estados/${ufValue}/municipios?orderBy=nome`
    
    // reseta o conteúdo do campo e da exibição para seleção
    citySelect.innerHTML = "<option value>Selecione a cidade</option>"
    citySelect.disabled = true;
    
    fetch(url)
    .then( res => res.json() )  // funcao anonima nao precisa das chaves, return e parenteses extras
    .then( cities =>
    {
        for (const city of cities)
        {
            citySelect.innerHTML += `<option value="${city.nome}">${city.nome}</option>`
        }
        
        citySelect.disabled = false;
    } )
}

populateUFs()

document
    .querySelector("select[name=uf]")
    .addEventListener("change", getCities)


/* Itens de coleta */

const itemsToCollect = document.querySelectorAll(".items-grid li")

for (const item of itemsToCollect)
{
    item.addEventListener("click", handleSelectedItem)
}


// atualizar o campo escondido com os itens selecionados
const collectedItems = document.querySelector("input[name=items]")

let selectedItems = []


function handleSelectedItem(event)
{
    const itemLi = event.target
    
    // adicionar ou remover uma classe com javascript
    itemLi.classList.toggle("selected")
    
    const itemId = itemLi.dataset.id
    console.log()
    
    // verificar se existem itens selecionados, se sim
    // pegar os itens selecionados
    const alreadySelected = selectedItems.findIndex( item =>
    {
        return item == itemId
    } )
    
    // se já estiver selecionado, tirar da seleção
    if(alreadySelected >= 0)
    {
        // tirar da seleção
        const filteredItems = selectedItems.filter( item =>
        {
            return item != itemId
        })
        
        selectedItems = filteredItems
    }
    else
    {
        // se não estiver selecionado, adicionar à seleção
        selectedItems.push(itemId)
    }
    
    collectedItems.value = selectedItems
}
